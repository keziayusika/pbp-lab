from django.shortcuts import render
from django.http.response import HttpResponse 
from django.core import serializers
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')

# Create your views here.
def index(request) :
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request) :
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
  
    context['form']= form
    return render(request, "lab3_form.html", context)