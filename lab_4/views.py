# Create your views here.
from django.shortcuts import render
from django.http.response import HttpResponse 
from django.core import serializers
from .forms import NoteForm
from django.contrib.auth.decorators import login_required
from lab_2.models import Note

@login_required(login_url='/admin/login/')

# Create your views here.
def index(request) :
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)

def add_note(request) :
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
  
    context['form']= form
    return render(request, "lab4_form.html", context)


def note_list(request) :
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)